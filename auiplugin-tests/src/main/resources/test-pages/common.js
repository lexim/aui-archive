/**
 * Common JS for test pages only.
 * Do not load resources for test pages - they must be viewed in the refapp.
 */
(function () {

    window.onload = function() {
        if (window.location.protocol == 'file:') {
            //var header = document.getElementById("bootcamp-header") || document.getElementById("test-header");
            var header = document.getElementById("bootcamp-header") || document.getElementsByTagName("body")[0];
            header.innerHTML = 'Error: this page must be viewed via the refapp: <a href="http://localhost:9999/ajs/">http://localhost:9999/ajs</a>';
            header.style.color = "red";
        }
    };
   
})();
 
AUITEST = AUITEST || {};

var AUITEST = {

    // DRY, innit. (note this bit of JS predates Soy :))
    newTestDropdown: function (id,ddType) {
        var base = jQuery('<div class="aui-dropdown2 aui-style-default"><ul><li><a href="#">Attach File</a></li><li><a href="#">Comment</a></li><li><a href="#">Edit Issue</a></li><li><a href="#">Watch Issue</a></li></ul></div>'),
            type = ddType || 'generic';
            
        if ( type == "unstyled") {
            base.removeClass("aui-style-default");
        }
        if ( type == "forcedwidth") {
            base.addClass("test-forced-width");
        }
        
        base.attr("id",id).appendTo("body");
        return base;
    },

    genericAlert: function () {
        alert("Dummy content clicked.");
    },

    disableTriggers: function () {
        var $triggers = AJS.$(".auitest-disabletriggers a, .auitest-disabletriggers button");
        $triggers.on("click", function(e) {
            AUITEST.genericAlert();
            return false;
        });
    },

    focusedPageLayoutToggles: function () {
        var $triggers = AJS.$("#aui-page-focused-small, #aui-page-focused-medium, #aui-page-focused-large, #aui-page-focused-xlarge"),
            bodyClass;
        $triggers.on("click", function(e) {
            bodyClass = "aui-layout aui-theme-default aui-page-focused " + AJS.$(this).attr("id");
            console.log(bodyClass);
            AJS.$("body").attr("class", bodyClass);
            return false;
        });
    }

};

AJS.$(document).ready(function(){
    AUITEST.disableTriggers();
    AUITEST.focusedPageLayoutToggles();
});