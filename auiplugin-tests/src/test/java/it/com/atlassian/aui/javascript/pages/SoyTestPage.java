package it.com.atlassian.aui.javascript.pages;

/**
 * @since 4.0
 */
public class SoyTestPage extends TestPage
{
    public String getUrl()
    {
        return "/plugins/servlet/ajstest/test-pages/soy/soy-test.html";
    }
}
