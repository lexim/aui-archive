package it.com.atlassian.aui.javascript.integrationTests;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.DropdownTestPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AUIDropDownTest extends AbstractAuiIntegrationTest
{

    private DropdownTestPage dropdownTestPage;
    private PageElementFinder elementFinder;

    @Before
    public void setup()
    {
        dropdownTestPage = product.visit(DropdownTestPage.class);
        elementFinder = dropdownTestPage.getElementFinder();
    }

    //Test to make sure dropdowns show correctly after being clicked.
    @Test
    public void testAUIDropDownShow()
    {
        PageElement dropdownTrigger = elementFinder.find(By.cssSelector("#dropDown-standard .aui-dd-trigger"));
        PageElement dropdown = elementFinder.find(By.cssSelector("#dropDown-standard .aui-dropdown"));
        dropdownTrigger.click();
        assertTrue("Dropdown should be visible as it was opened.", dropdown.isVisible());
    }

    //test to make sure dropdowns hide after a click on the body
    @Test
    public void testAUIDropDownHide()
    {
        PageElement dropdownTrigger = elementFinder.find(By.cssSelector("#dropDown-standard .aui-dd-trigger"));
        PageElement dropdown = elementFinder.find(By.cssSelector("#dropDown-standard .aui-dropdown"));
        dropdownTrigger.click();
        assertTrue("Dropdown should be visible as it was opened.", dropdown.isVisible());

        elementFinder.find(By.tagName("body")).click();

        assertFalse("Dropdown should not be visible as there was an external click", dropdown.isVisible());
    }

    @Test
    public void testDropdownLeftAlign()
    {
        PageElement dropdownTrigger = elementFinder.find(By.cssSelector("#dropDown-left .aui-dd-trigger"));
        WebDriverElement dropdown = (WebDriverElement) elementFinder.find(By.cssSelector("#dropDown-left .aui-dropdown"));
        dropdownTrigger.click();
        assertTrue("Dropdown should be visible as it was opened.", dropdown.isVisible());

        WebElement dropdownWebElement = dropdown.asWebElement();

        assertEquals("The left alignment should be 0px", "0px", dropdownWebElement.getCssValue("left"));
    }

    @Test
    public void testDropdownRightAlign()
    {
        PageElement dropdownTrigger = elementFinder.find(By.cssSelector("#dropDown-right .aui-dd-trigger"));
        WebDriverElement dropdown = (WebDriverElement) elementFinder.find(By.cssSelector("#dropDown-right .aui-dropdown"));
        dropdownTrigger.click();
        assertTrue("Dropdown should be visible as it was opened.", dropdown.isVisible());

        WebElement dropdownWebElement = dropdown.asWebElement();

        assertEquals("The right alignment should be 0px", "0px", dropdownWebElement.getCssValue("right"));
    }

    @Test
    public void testDropdownDisabled()
    {
        PageElement dropdownTrigger = elementFinder.find(By.cssSelector("#dropDown-disabled .aui-dd-trigger"));
        WebDriverElement dropdown = (WebDriverElement) elementFinder.find(By.cssSelector("#dropDown-disabled .aui-dropdown"));
        dropdownTrigger.click();
        assertFalse("Dropdown should not be visible as it is disabled.", dropdown.isVisible());

        elementFinder.find(By.id("disabledToggle")).click();

        dropdownTrigger.click();
        assertTrue("Dropdown should be visiable as it is not disabled.", dropdown.isVisible());

    }
}
