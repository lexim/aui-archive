package it.com.atlassian.aui.javascript.integrationTests.experimental;


import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import org.junit.Test;

/**
 * Example test for experimental components
 * @since 4.0
 */
public class AUIExperimentalExampleTest extends AbstractAuiIntegrationTest
{
    @Test
    public void testNothing()
    {

    }
}
