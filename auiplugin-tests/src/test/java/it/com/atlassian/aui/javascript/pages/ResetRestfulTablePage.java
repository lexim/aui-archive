package it.com.atlassian.aui.javascript.pages;

import com.atlassian.pageobjects.Page;

/**
 * Need a clean slate? Remove the data in the restfultable before using it.
 */
public class ResetRestfulTablePage implements Page
{
    public String getUrl()
    {
        return "/rest/contacts/1.0/contacts/reset";
    }
}
