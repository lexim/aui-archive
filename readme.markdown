# AUI

AUI is the Atlassian User Interface library. It is open source; and provided both as a plugin for use in Atlassian products and a static file 'flat pack'. Note the flat pack does not contain those AUI components which rely on backend functionality.

## Demos

The [Atlassian Design Guidelines](https://developer.atlassian.com/design/) are built with AUI and provide working demos and design principles for creating an Atlassian UI.

## Documentation
* [Component documentation](http://developer.atlassian.com/display/AUI/)
* [Sandbox Tool](http://docs.atlassian.com/aui/latest/sandbox/)
* [Release Notes](https://developer.atlassian.com/display/AUI/AUI+Release+Notes)

## Quick Start Guide - Static files

If you want to do quick prototypes or deploy AUI using static files, [see the latest release notes for a 'flat pack' download](https://developer.atlassian.com/display/AUI/AUI+Release+Notes). You can simply unzip the flat pack and get started.

## Quick Start Guide - Atlassian Plugin

Most Atlassian products already provide AUI, ready for use. Check the version by running "AJS.version" in your browser console. If it is not available, you will need to refer to [Adding AUI To Your Application](https://developer.atlassian.com/display/AUI/Adding+AUI+to+your+application) in the docs.

## AUI development

AUI's own build uses the Atlassian reference application (REFAPP) for development and testing. There is a [guide to setting up an AUI development environment](https://developer.atlassian.com/display/AUI/Setting+up+a+dev+environment+for+AUI) in the docs.

See the [AUI Contributor Guide](https://developer.atlassian.com/display/AUI/AUI+Contributor+Guide) for more details about contributing code to AUI.

## License

AUI is open source, licensed under the Apache License Version 2.0. See file comments and "licenses" directory for more license information about AUI and included libraries.