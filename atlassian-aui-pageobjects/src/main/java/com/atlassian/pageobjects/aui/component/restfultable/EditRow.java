package com.atlassian.pageobjects.aui.component.restfultable;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

public class EditRow
{
    private final PageElement row;

    public EditRow(final PageElement row)
    {
        this.row = row;
    }

    @Inject
    private PageElementFinder finder;

    public EditRow fill(final String... fields)
    {

        for (int i=0; i < fields.length; i=i+2)
        {
            row.find(By.name(fields[i])).type(fields[i+1]);
        }

        return this;
    }

    public String getValue(final String field)
    {
        return row.find(By.name(field)).getValue();
    }

    public EditRow setFieldValue(final String field, final String value) {
        row.find(By.name(field)).type(value);
        return this;
    }

    public Map<String, String> getValidationErrors()
    {
        final List<PageElement> errorEls = row.findAll(By.className("error"));
        Map<String, String> errors = new HashMap<String, String>();
        for (PageElement errorEl : errorEls)
        {
            errors.put(errorEl.getAttribute("data-field"), errorEl.getText());
        }
        return errors;
    }

    public PageElement getSubmitButton()
    {
        return row.find(By.cssSelector(".aui-button[type=submit]"));
    }

    public PageElement getCancelLink()
    {
        return row.find(By.className("aui-restfultable-cancel"));
    }

    public EditRow submit()
    {
        getSubmitButton().click();
        waitUntilFalse(row.timed().hasClass("loading"));
        return this;
    }

    public void cancel()
    {
        getCancelLink().click();
        waitUntilFalse(row.timed().hasClass("loading"));
        waitUntilFalse(getCancelLink().timed().isPresent());
    }
}
