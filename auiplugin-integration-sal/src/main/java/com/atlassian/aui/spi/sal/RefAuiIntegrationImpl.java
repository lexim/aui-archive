package com.atlassian.aui.spi.sal;

import com.atlassian.aui.spi.AuiIntegration;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.sal.api.message.I18nResolver;

import javax.servlet.ServletContext;
import java.io.Serializable;

public class RefAuiIntegrationImpl implements AuiIntegration {

    private final ServletContext servletContext;
    private final I18nResolver i18nResolver;

    public RefAuiIntegrationImpl(ServletContextFactory servletContextFactory, I18nResolver i18nResolver) {
        this.servletContext = servletContextFactory.getServletContext();
        this.i18nResolver = i18nResolver;
    }

    public String getContextPath() {
        return this.servletContext.getContextPath();
    }

    public String getRawText(String key) {
        return this.i18nResolver.getRawText(key);
    }

    public String getText(String key, Serializable... arguments) {
        return this.i18nResolver.getText(key, arguments);
    }
}
